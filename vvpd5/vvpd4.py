def intersection(x1, y1, x2, y2, x3, y3, x4, y4):
    """
    Определяет, сколько квадратных единиц составляет площадь пересечения двух прямоугольников
    :type x1, y1, x2, y2, x3, y3, x4, y4: int
    :param x1, y1, x2, y2, x3, y3, x4, y4: координаты 
    :return: 0, если (width < 0) | (height < 0), иначе width * height
    """
    left = max(x1, x3)
    right = min(x2, x4)
    top = min(y2, y4)
    bottom = max(y1, y3)
    width = right - left
    height = top - bottom
    if (width < 0) | (height < 0):
        return 0
    else:
        return width * height

def union(x1, y1, x2, y2, x3, y3, x4, y4):
    """
    Определяет, сколько квадратных единиц составляет площадь объединения двух прямоугольников
    :type x1, y1, x2, y2, x3, y3, x4, y4: int
    :param x1, y1, x2, y2, x3, y3, x4, y4: координаты 
    :return: s1 + s2 - intersection(x1, y1, x2, y2, x3, y3, x4, y4)
    """
    s1 = (x1 - x2) * (y1 - y2)
    s2 = (x3 - x4) * (y3 - y4)
    return s1 + s2 - intersection(x1, y1, x2, y2, x3, y3, x4, y4)