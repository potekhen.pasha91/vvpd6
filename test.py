import pytest
from vvpd5 import vvpd4


@pytest.fixture(autouse=True)
def test_start():
    print('Запуск теста')


def test_intersection_1():
    assert vvpd4.intersection(1,1,4,4,2,2,7,7) == 4


def test_intersection_2():
    assert vvpd4.intersection(1,1,4,4,5,5,7,7) == 0


@pytest.mark.parametrize('x1, x2, x3, x4, x5, x6, x7, x8, result',[(1,1,4,4,0,0,-1,-1, 0),
                                                                   (1,1,4,4,0,0,8,8,9)])
def test_intersection_3(x1, x2, x3, x4, x5, x6, x7, x8, result):
    assert vvpd4.intersection(x1, x2, x3, x4, x5, x6, x7, x8) == result


@pytest.mark.parametrize('x1, x2, x3, x4, x5, x6, x7, x8, result',[(1,1,4,4,2,2,4,4,9),
                                                                   (1,1,4,4,2,2,2,2,9),
                                                                   (1,1,4,4,2,2,8,8,41)])
def test_union_1(x1, x2, x3, x4, x5, x6, x7, x8, result):
    assert vvpd4.union(x1, x2, x3, x4, x5, x6, x7, x8) == result


def test_union_2():
    assert vvpd4.union(1,1,4,4,6,6,8,8) == 13