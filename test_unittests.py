from unittest import TestCase
from vvpd5 import vvpd4


class TestMain(TestCase):
    def start(self):
        print('Запуск теста')
    

    def end(self):
        print('Окончание теста')
    

    def test_first_func_1(self):
        self.assertEqual(vvpd4.intersection(1,1,4,4,2,2,7,7), 4)
    

    def test_first_func_2(self):
        self.assertEqual(vvpd4.intersection(1,1,4,4,5,5,7,7), 0)
    

    def test_first_func_3(self):
        self.assertEqual(vvpd4.intersection(1,1,4,4,0,0,8,8), 9)


    def test_second_func_1(self):
        self.assertEqual(vvpd4.union(1,1,4,4,2,2,2,2), 9)

    
    def test_second_func_2(self):
        self.assertEqual(vvpd4.union(1,1,4,4,6,6,8,8), 13)